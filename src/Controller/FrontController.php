<?php

namespace App\Controller;

use App\Repository\DecidableRepository;
use App\Repository\DecisionRepository;
use App\Repository\SpeakerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
* @Route("", name="front_")
*/

class FrontController extends AbstractController
{
    /**
     * @Route("/", name="index")
     * @param SpeakerRepository $speakerRepository
     * @param DecidableRepository $decidableRepository
     * @param DecisionRepository $decisionRepository
     * @return Response
     */
    public function index(SpeakerRepository $speakerRepository,
                          DecidableRepository $decidableRepository,
                          DecisionRepository $decisionRepository): Response
    {
        $speakers =  $speakerRepository->findAll();
        $decidables = $decidableRepository->findAll();
        $decisions = $decisionRepository->findAll();

        return $this->render('front/index.html.twig',
            compact(['speakers', 'decidables', 'decisions'])
        );
    }
}
