<?php

namespace App\Controller\Admin;

use App\Entity\Decision;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class DecisionCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {

        return Decision::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('speaker'),
            AssociationField::new('decidable'),
        ];
    }

}
