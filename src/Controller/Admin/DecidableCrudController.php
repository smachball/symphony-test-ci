<?php

namespace App\Controller\Admin;

use App\Entity\Decidable;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class DecidableCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Decidable::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
