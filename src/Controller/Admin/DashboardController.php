<?php

namespace App\Controller\Admin;

use App\Entity\Decidable;
use App\Entity\Decision;
use App\Entity\Speaker;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
/**
 * @Route("/admin", name="admin_")
 */
class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/", name="dashboard")
     */
    public function index(): Response
    {
        return parent::index();
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('BingoMacron');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Speaker', 'fas fa-user', Speaker::class);
        yield MenuItem::linkToCrud('Hypothèses', 'fas fa-question', Decidable::class);
        yield MenuItem::linkToCrud('Decision', 'fas fa-user', Decision::class);
    }
}
