<?php

namespace App\Repository;

use App\Entity\Decidable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Decidable|null find($id, $lockMode = null, $lockVersion = null)
 * @method Decidable|null findOneBy(array $criteria, array $orderBy = null)
 * @method Decidable[]    findAll()
 * @method Decidable[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DecidableRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Decidable::class);
    }

    // /**
    //  * @return Decidable[] Returns an array of Decidable objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Decidable
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
