<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210331125156 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE decidable (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(255) NOT NULL, desciption VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE decision (id INT AUTO_INCREMENT NOT NULL, speaker_id INT DEFAULT NULL, decidable_id INT DEFAULT NULL, created_at DATETIME NOT NULL, INDEX IDX_84ACBE48D04A0F27 (speaker_id), INDEX IDX_84ACBE484E74E5F0 (decidable_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE decision ADD CONSTRAINT FK_84ACBE48D04A0F27 FOREIGN KEY (speaker_id) REFERENCES speaker (id)');
        $this->addSql('ALTER TABLE decision ADD CONSTRAINT FK_84ACBE484E74E5F0 FOREIGN KEY (decidable_id) REFERENCES decidable (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE decision DROP FOREIGN KEY FK_84ACBE484E74E5F0');
        $this->addSql('DROP TABLE decidable');
        $this->addSql('DROP TABLE decision');
    }
}
